### Porquê?

Existem diversos programas para Android que fazem o mesmo que o Simple AdAway faz, mas qual é o problema? Cada um desses programas são voltados apenas para um funções/finalidades específicas. Comecei a desenvolver o Simple AdAway justamente para contornar esses "problemas", pois eu queria ter mais liberdade para executar determinadas funções e operações. No Simple AdAway, inclui opções e funcionalidades que eu gostaria de ter usado em outros programas, mas que não pude usar porque aquele determinado programa simplesmente era limitado ou porque aquela opção nem ao menos existia.

_"Porque usar vários programas para um mesmo propósito?"_

O  Simple AdAway é voltado para "usuários finais" e para pessoas que, assim como eu, querem ter mais liberdade.

### Projeto original?

Tecnicamente, não. Como citado anteriormente, há uma porção de programas lançados para diversas plataformas que fazem basicamente a mesma coisa que o Simple AdAway faz.

A maioria das funções que foram adicionadas ao Simple AdAway são provenientes de outros programas.

### Então você copia coisas de outros programas?! Maldito plagiador!

Não. Embora o Simple AdAway seja baseado em outros programas, nenhuma função ou outros traços de código foram copiado de programas de terceiros. Tudo foi desenvolvido a partir do 0.

### "Bloqueador"?

Embora o Simple AdAway seja categorizado como um bloqueador de anúncios, ele não é.

O Simple AdAway não manuseia, intercepta ou filtra diretamente as conexões realizadas pelo dispositivo. O Simple AdAway apenas realiza o uso de programas (binários) disponíveis no próprio sistema Android para manusear o comportamento das conexões realizadas pelo mesmo.

Para "bloquear" hosts, o Simple AdAway utiliza o [ficheiro de hosts](http://pt.wikipedia.org/wiki/Hosts_(arquivo)) do sistema e os binarios de gerenciamento de regras de tráfego do Kernel, [iptables](http://pt.wikipedia.org/wiki/Iptables) e ip6tables.

"Bloqueador" é só um nome chamativo. Considere-o como um pseudônimo.
