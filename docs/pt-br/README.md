<h2 align='center'>Simple AdAway para Android</h2>
<div align="center">
	<img src="http://img.shields.io/badge/Vers%C3%A3o-v0.5-brightgreen.svg?style=flat" alt="Versão atual" />
	 <img src="http://img.shields.io/badge/Atualizado-Oct%2007,%202019-important.svg?style=flat" alt="Data da última atualização " />
	 <img src="http://img.shields.io/badge/Status-Inst%C3%A1vel-critical.svg?style=flat" alt="Status da versão atual " />
</div> 

### Clone este repositório:

* `git clone -b 'master' 'http://github.com/SnwMds/SimpleAdAway.git'`

### Baixe este repositório:

* `wget --no-check-certificate 'codeload.github.com/SnwMds/SimpleAdAway/zip/master' -O 'SimpleAdAway.zip'`

ou

* [codeload.github.com/SnwMds/SimpleAdAway/zip/master](http://codeload.github.com/SnwMds/SimpleAdAway/zip/master)

### Licença para uso e redistribuição:

* [GNU General Public License v3.0](http://gnu.org/licenses/gpl-3.0)

### Contribuições

* [Informações sobre como contribuir](http://opensource.guide/pt/how-to-contribute/#por-que-contribuir-para-o-open-source)

### Requisitos

* Ambiente operacional Android
* Acesso ao usuário root
* [BusyBox](http://forum.xda-developers.com/attachment.php?attachmentid=4751649&d=1556720721)
* [Coreutils](http://zackptg5.com/downloads/GNU-Utils-Android_v2.1.zip) (é apenas um complemento)
* Conexão estável com a internet
* Suporte a scripts init.d, su.d ou service.d (também funciona sem)

### Requisitos para execução em modo systemless

* Todos os outros requisitos acima
* [Magisk v19.0](http://raw.githubusercontent.com/topjohnwu/magisk_files/master/canary_builds/magisk-debug.zip) ou superior
* Systemless Hosts Module (ative nas configurações do Magisk Manager)

### Principais funcionalidades

* Bloqueia hosts relacionadas a anúncios, rastreamento online e malware
* Possibilita bloquear/desbloquear hosts e endereços IPv4/IPv6
* Possibilita a alteração de DNS (IPv4/IPv6)
* Permite buscar e ver informações sobre hosts e endereços IPv4/IPv6 (APIs externas)
* Possui um menu interativo
* Também disponhe um modo não interativo

O Simple AdAway também é totalmente customizável. Você pode tanto usar as configurações padrões, bem como aplicar suas próprias regras.

### O que é isso?

O Simple AdAway é um shell script, o mesmo é baseado no [AdAway](http://adaway.org), um aplicativo para [Android](http://android.com) capaz de bloquear anúncios utilizando o [ficheiro de hosts](http://pt.wikipedia.org/wiki/Hosts_(arquivo)) do sistema.

### Qual é a utilidade?

Como funcionalidade principal, o Simple AdAway é capaz de realizar o bloqueio de servidores de [anúncios](http://pt.wikipedia.org/wiki/Publicidade_online), [analytics](http://pt.wikipedia.org/wiki/Web_analytics) e [malwares](http://pt.wikipedia.org/wiki/Malware). O Simple AdAway também funciona como um alterador de DNS, que possibilita que o usuário altere os servidores DNS (IPv4/IPv6) padrões do sistema.

### Como instalar?

_**Aviso:** Para executar o Simple AdAway em modo systemless, você precisará do Magisk._

Você poderá executar o Simple AdAway tanto como um executável do sistema, quanto em uma execução direta. Se você deseja instalar o Simple AdAway direto em seu sistema, siga as instruções exibidas à seguir. Caso contrário, pule para a [próxima explicação](#execu%C3%A7%C3%A3o-direta).

Você precisará realizar a instalação do módulo do Simple AdAway para que ele se torne um executável do sistema. Para que isso ocorra, faça o [download](http://github.com/SnwMds/SimpleAdAway/releases/latest) do arquivo de instalação e, após salvo em seu armazenamento interno, realize a instalação através de uma das duas maneiras citadas à seguir:

<h4 align='center'>Através do Magisk Manager (requer o Magisk)</h4>

1. Clique no **menu de opções** no canto superior esquerdo
2. Vá até a aba **Módulos**
3. Clique no ícone com um sinal de **+** para adicionar um novo módulo.
4. Procure pelo [arquivo de instalação](http://github.com/SnwMds/SimpleAdAway/releases/latest) que você baixou anteriormente
5. Selecione-o e espere até que a instalação seja concluída
6. Reinicie seu dispositivo após isso

<h4 align='center'>Através do Modo Recovery (TWRP/CWM)</h4>

1. Procure por alguma opção que indique algo como **INSTALAR** e selecione-a
2. Procure pelo [arquivo de instalação](http://github.com/SnwMds/SimpleAdAway/releases/latest) que você baixou anteriormente
3. Selecione-o e confirme a instalação
4. Reinicie seu dispositivo após isso

A partir de agora, o script executável do Simple AdAway já deverá estar instalado em seu sistema.

### Execução direta

Para executar o script de forma direta, sem a necessidade do [Magisk](http://github.com/topjohnwu/Magisk), [TWRP](http://twrp.me) ou [CWM](http://forum.xda-developers.com/wiki/ClockworkMod_Recovery), você precisará realizar o download do script e executa-lo diretamente no terminal. Para isso, entre em um emulador de terminal com acesso ao [usuário root](http://pt.wikipedia.org/wiki/Root_no_Android) e execute a seguinte linha de comandos:

`wget --no-check-certificate 'raw.githubusercontent.com/SnwMds/SimpleAdAway/master/system/bin/sad' -O '/sdcard/sad' && sh '/sdcard/sad'`

A linha de comandos exibida acima irá realizar o download do script executável do Simple AdAway, irá salva-lo em seu armazenamento interno e irá executa-lo logo em seguida, sem a necessidade de mais interações.

_**Aviso:** Não faça o download de versões instáveis, elas podem não funcionar corretamente e/ou podem acabar danificando seu sistema._

### Parâmetros

Alguns parâmetros podem ser adicionados ao Simple AdAway para que ele realize/execute operações específicas. Observe logo abaixo uma lista de todos os parâmetros disponíveis e suas respectivas funcionalidades.

* [Lista da parâmetros disponíveis no Simple AdAway](PARAMETERS.md#par%C3%A2metros-b%C3%A1sicos)

### Execução com parâmetros

Se o Simple AdAway for um executável do sistema, utilize da seguinte forma:

**Exemplo:** `sad --all --use-cloudflare-dns`

- `sad` é o o script executável do Simple AdAway em seu sistema.  
- `--all` e`--use-cloudflare-dns` são parâmetros.  

Se você estiver utilizando o Simple AdAway em modo de execução direta (isto é, ele está armazenado em sua memória interna), você precisará especificar o seu [caminho de execução](#caminho-de-execu%C3%A7%C3%A3o) seguido do parâmetro.

**Exemplo:** `sh '/sdcard/sad' --all --use-cloudflare-dns`

- `sh` é o interpretador de comandos padrão.  
- `'/sdcard/sad'` é o script executável do Simple AdAway em seu armazenamento interno.  
- `--all` e `--use-cloudflare-dns` são parâmetros.  

### Caminho de execução

 Se o script não é um executável do sistema, você precisará especificar o seu caminho de execução.

**Exemplo:** `sh '/sdcard/sad'`

`sh` é o interpretador de comandos padrão. `/sdcard` é o caminho do armazenamento interno. `sad` é o script em sí. Se específico `/sdcard/sad` como caminho de execução, estou dizendo ao shell que o script está no armazenamento interno e que o nome do arquivo é `sad`. É a partir dali que ele precisa ser executado. É necessário fazer exatamente desta forma para que o shell detecte onde ele está e o execute-o sem problemas.

Se você realizou a instalação do arquivo zip do Simple AdAway, isto significa que agora ele é um executável do sistema. Desta forma, você não precisará especificar um caminho para ele sempre que for executa-lo. Em vez disso, digite apenas `sad` no terminal para usá-lo.

### Considerações finais

Se você tem alguma dúvida ou quer falar sobre qualquer outra coisa relacionada ao projeto, abra uma [issue](http://github.com/SnwMds/SimpleAdAway/issues/new) neste repositório.

Se você tiver alguma dúvida, sugestão, crítica, e/ou qualquer outra coisa que queira me dizer, entre em contato comigo via [e-mail](http://scr.im/SnwMds) ou através do [Telegram](http://t.me/SnwMds).

### Créditos e agradecimentos

Obrigado ao John Wu ([topjohnwu](http://github.com/topjohnwu)) por criar e disponibilizar o [Magisk](http://github.com/topjohnwu/Magisk) e o [Magisk Manager](http://github.com/topjohnwu/MagiskManager).

Obrigado ao [ahrion](http://github.com/therealahrion) e ao [Zackptg5](http://github.com/Zackptg5) por criarem e disponibilizarem o [Unity (Un)Installer Template](http://github.com/Zackptg5/Unity).

Obrigado a todas as pessoas que disponibilizaram as [fontes de hosts](THIRD-PARTY.md#lista-de-filtros-do-simple-adaway) que são utilizadas no Simple AdAway.

E um imenso obrigado ao [usuários que contribuíram](CONTRIBUTORS.md#contribuidores) com este projeto!
