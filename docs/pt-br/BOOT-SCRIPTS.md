<h3 align='center'>Scripts durante a inicialização</h3>

Para que você possa usufruir de todos os benefícios do Simple AdAway, sua ROM ou Kernel deverá oferecer suporte a execução de scripts durante a inicialização.

<h4 align='center'>O que é isso?</h4>

Scripts de inicialização são linhas de comando que são executadas quando o dispositivo está reiniciando ou quando esta sendo ligado. O Simple AdAway usa isto para reaplicar algumas regras que são resetadas durante o reinicio do sistema.

<h4 align='center'>Porque isso é necessário no Simple AdAway?</h4>

O Simple AdAway utiliza o [iptables](http://pt.wikipedia.org/wiki/Iptables) e o [ip6tables](http://pt.wikipedia.org/wiki/Ip6tables) para aplicar diversos tipos de regras, tais como: bloqueio/desbloqueio de endereços IPv4/IPv6, bloqueio/desbloqueio de hosts (regras personalizadas) e alteração de DNS. Infelizmente, todas as regras do iptables são resetadas pelo sistema quando o dispositivo é reiniciado, fazendo assim com que seja sempre necessário reaplicalas.

O SuperSU e o Magisk oferecem meios próprios para executar scripts durante a inicialização, portanto, se você utiliza um desses 2 programas, não é necessário que o seu sistema ofereça suporte nativo ao init.d.

Se você não utiliza o Magisk ou o SuperSU como programa de gerenciamento de superusuário, sua ROM ou Kernel deverá fornecer suporte nativo ao init.d para que o Simple AdAway possa executar scripts durante a reinicialização.

<h4 align='center'>É dispensável?</h4>

O Simple AdAway ainda poderá funcionar mesmo sem suporte a scripts de inicialização, a única diferença é que, caso você tenha o usado para aplicar regras personalizadas (blacklist/whitelist) e/ou para alterar o DNS, essas configurações serão perdidas após reiniciar o dispositivo. Você sempre precisará reaplicalas manualmente.
