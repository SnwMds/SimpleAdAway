<h3 align='center'>Modos de aplicação das hosts</h3>

O Simple AdAway possuí 2 formas de aplicação: systemless e não-systemless. O modo de aplicação não-systemless modifica diretamente a partição `/system`. Já a aplicação em modo systemless não realiza modificações diretas. 

Caso você esteja utilizando o Magisk como programa padrão de gerenciamento de superusuário, as hosts serão automaticamente aplicadas em modo systemless. Caso contrário, as hosts serão automaticamente aplicadas em modo não-systemless.

_**Nota:** Devido a certas alterações realizadas a partir versão 17.4 do [Magisk Canary](http://forum.xda-developers.com/apps/magisk/dev-magisk-canary-channel-bleeding-edge-t3839337), não é mais possível modificar diretamente o ficheiro de hosts do sistema, sendo assim necessário ativar o módulo de hosts systemless nas configurações do [Magisk Manager](http://forum.xda-developers.com/apps/magisk) para que o Simple AdAway possa ser capaz de realizar modificações indiretas neste arquivo._
