<h3 align='center'>Possíveis problemas</h3>

O Simple AdAway sempre é testado antes de ser publicado, mas como nenhum software está livre de problemas, abaixo eu especifiquei certas "coisas inesperadas" que podem vir à ocorrer durante a execução e como tentar resolvê-las.

_**Aviso:** As soluções listadas abaixo não são efetivas, nada garante que elas vão (de fato) resolver seu problema. Caso necessite de ajuda adicional, [entre em contato](#Ajuda%20adicional) comigo._

- **I could not get root access!**

Descrição: Este problema geralmente surge quando o Simple AdAway não encontrou ou não pôde obter acesso ao root.

Possível solução: Verifique se o acesso root foi concedido ao terminal, verifique também se o terminal possui acesso ao usuário root.

- **I could not get busybox access!**

Descrição: Este problema ocorre quando o Simple AdAway não conseguiu encontrar e/ou obter acesso ao busybox presente em seu sistema.

Possível solução: instale o [BusyBox](http://forum.xda-developers.com/attachment.php?attachmentid=4751649&d=1556720721) em seu sistema.

- **I could not get internet access!**

Descrição: Este problema ocorre quando o Simple AdAway não conseguiu encontrar e/ou obter acesso a internet.

Possível Solução: verique se você possui uma conexão estável com a internet. Se você está usando a internet através de um firewall, verifique se o servidor "[209.51.188.148](http://209.51.188.148)" está ou não bloqueado nele. Se estiver bloqueado, remova-o ou desative temporariamente seu firewall. Este servidor é utilizado para verificar sua conexão com a internet.

- **I could not get access to internal storage!**

Descrição: Este problema geralmente surge quando o Simple AdAway não conseguiu obter acesso ao armazenamento interno.

Possível solução: Verique se as permissões _READ_EXTERNAL_STORAGE_ e _WRITE_EXTERNAL_STORAGE_ estão concedidas ao aplicativo que você utiliza como emulador de terminal.

- **Fail! No files downloaded!**

Descrição: Este problema geralmente ocorre quando nenhum dos arquivos pôde ser baixado.

Possível solução: Este problema surge na maioria das vezes por conta de problemas de conexão com a internet. Com isto, bastaria apenas que você verificasse e resolvesse seu problema de conexão para que o problema fosse resolvido. 

Solução alternativa: Se a "solução" acima não resolver seu problema, verifique se você está utilizando o SuperSU como programa padrão de gerenciamento de superusuário, se sim, entre nas configurações do SuperSU, desative a opção "mount namespace separation" e reinicie seu dispositivo. O Simple AdAway agora deverá funcionar corretamente.

- **Permission denied**

Descrição: Este problema geralmente surge quando o Simple AdAway não possuí permissões suficientes para que possa realizar/executar uma determinada operação.

Possível solução: Verifique se o acesso root foi concedido ao terminal, verifique também se o terminal possui acesso ao usuário root.

- **Command not found**

Descrição: Este problema ocorre quando o Simple AdAway não conseguiu encontrar um ou mais componentes necessários para executar uma determinada operação.

Possível solução: Instale o [BusyBox](http://forum.xda-developers.com/attachment.php?attachmentid=4751649&d=1556720721) ou o [Coreutils](http://zackptg5.com/downloads/GNU-Utils-Android_v2.1.zip) em seu sistema.

- **No such file or directory**

Descrição: Este problema surge quando o Simple AdAway não conseguiu encontrar um determinado arquivo ou diretório.

Possível solução: Se você estiver tentando executar o Simple AdAway de forma direta e está recebendo este aviso, isto significa que o caminho que você especificou para ele estava incorreto e/ou não existia, com isso, tente informar o caminho de execução correto.

- **$HostsPath was returned a invalid result**

Descrição: Este problema ocorre quando a variável '$HostsPath' retorna um valor que não era esperado.

Possível solução: Este é um problema genérico, significa que não há uma solução específica para ele. Se este problema ocorrer com você, apenas tente executar o script novamente.

- **An error has occurred!**

Descrição: Este aviso pode ser exibido a você quando alguma função e/ou operação retornou um status de erro. Caso disponível, verifique o arquivo contendo os logs do Simple AdAway para que você descubra o que exatamente causou o erro.

Possível solução: Este é um problema genérico, significa que não há uma solução específica para ele. Se este problema ocorrer com você, apenas tente executar o script novamente.

<h3 align='center'>Ajuda adicional</h3>

Se você encontrou qualquer outro problema que não esteja listado aqui, precisa de ajuda a respeito de um assunto específico, ou quer falar sobre qualquer outra coisa relacionada ao projeto, abra uma [issue](http://github.com/SnwMds/SimpleAdAway/issues/new) neste repositório. Se preferir, veja [outras formas](README.md#Considera%C3%A7%C3%B5es%20finais) de entrar em contato.