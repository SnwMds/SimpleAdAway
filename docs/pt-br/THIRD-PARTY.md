<h3 align='center'>Lista de filtros do Simple AdAway</h3>

Abaixo está disponível uma lista de todos os filtros que estão atualmente sendo utilizados pelo Simple AdAway:

- **Goodbye Ads**
  - Autor: Jerry Joseph ([jerryn70](http://github.com/jerryn70))
  - Repositório: [jerryn70/GoodbyeAds](http://github.com/jerryn70/GoodbyeAds) (GitHub)
  - Licença: [MIT License](http://github.com/jerryn70/GoodbyeAds/blob/master/LICENSE)
  - Download: [raw.githubusercontent.com](http://raw.githubusercontent.com/jerryn70/GoodbyeAds/master/Hosts/GoodbyeAds.txt) (US)
  - Suporta TLS/SSL: Sim

- **MVPS Hosts**
  - Homepage: [winhelp2002.mvps.org](http://winhelp2002.mvps.org/hosts.htm)
  - Licença: [CC BY-NC-SA 3.0](http://creativecommons.org/licenses/by-nc-sa/3.0)
  - Download: [winhelp2002.mvps.org](http://winhelp2002.mvps.org/hosts.txt) (US)
  - Suporta TLS/SSL: Não

- **Dan Pollock's Hosts List**
  - Autor: [Dan Pollock](http://someonewhocares.org)
  - Homepage: [someonewhocares.org](http://someonewhocares.org/hosts)
  - Licença: Livre para copiar e distribuir para fins não comerciais
  - Download: [someonewhocares.org](http://someonewhocares.org/hosts/hosts) (CA)
  - Suporta TLS/SSL: Sim

- **Energized Protection - Spark**
  - Autor: Ador ([AdroitAdorKhan](http://github.com/AdroitAdorKhan))
  - Repositório: [EnergizedProtection/block](http://github.com/EnergizedProtection/block) (GitHub)
  - Licença: [CC-BY-SA-4.0](http://github.com/EnergizedProtection/block/blob/master/LICENSE)
  - Download: [raw.githubusercontent.com](http://raw.githubusercontent.com/EnergizedProtection/block/master/spark/formats/hosts.txt) (US)
  - Suporta TLS/SSL: Sim
  
- **Energized Protection - Social**
  - Autor: Ador ([AdroitAdorKhan](http://github.com/AdroitAdorKhan))
  - Repositório: [EnergizedProtection/block](http://github.com/EnergizedProtection/block) (GitHub)
  - Licença: [CC-BY-SA-4.0](http://github.com/EnergizedProtection/block/blob/master/LICENSE)
  - Download: [raw.githubusercontent.com](http://raw.githubusercontent.com/EnergizedProtection/block/master/extensions/social/formats/domains.txt) (US)
  - Suporta TLS/SSL: Sim
  
- **Energized Protection - Regional**
  - Autor: Ador ([AdroitAdorKhan](http://github.com/AdroitAdorKhan))
  - Repositório: [EnergizedProtection/block](http://github.com/EnergizedProtection/block) (GitHub)
  - Licença: [CC-BY-SA-4.0](http://github.com/EnergizedProtection/block/blob/master/LICENSE)
  - Download: [raw.githubusercontent.com](http://raw.githubusercontent.com/EnergizedProtection/block/master/extensions/social/formats/domains.txt) (US)
  - Suporta TLS/SSL: Sim
  
- **NSABlocklist | Trackers**
  - Autor: ([CHEF-KOCH](http://github.com/CHEF-KOCH))
  - Repositório: [CHEF-KOCH/NSABlocklist](http://github.com/CHEF-KOCH/NSABlocklist) (GitHub)
  - Licença: [ISC License](http://github.com/CHEF-KOCH/NSABlocklist/blob/master/LICENSE)
  - Download: [raw.githubusercontent.com](http://raw.githubusercontent.com/CHEF-KOCH/NSABlocklist/master/HOSTS/Trackers/trackers.txt) (US)
  - Suporta TLS/SSL: Sim
  
- **NSABlocklist | Fingerprinting**
  - Autor: ([CHEF-KOCH](http://github.com/CHEF-KOCH))
  - Repositório: [CHEF-KOCH/NSABlocklist](http://github.com/CHEF-KOCH/NSABlocklist) (GitHub)
  - Licença: [ISC License](http://github.com/CHEF-KOCH/NSABlocklist/blob/master/LICENSE)
  - Download: [raw.githubusercontent.com](http://raw.githubusercontent.com/CHEF-KOCH/NSABlocklist/master/HOSTS/Trackers/canvas%20fingerprinting%20pages.txt) (US)
  - Suporta TLS/SSL: Sim
  
- **NSABlocklist | WebRTC Trackers**
  - Autor: ([CHEF-KOCH](http://github.com/CHEF-KOCH))
  - Repositório: [CHEF-KOCH/NSABlocklist](http://github.com/CHEF-KOCH/NSABlocklist) (GitHub)
  - Licença: [ISC License](http://github.com/CHEF-KOCH/NSABlocklist/blob/master/LICENSE)
  - Download: [raw.githubusercontent.com](http://raw.githubusercontent.com/CHEF-KOCH/NSABlocklist/master/HOSTS/Trackers/webrtc%20tracking.txt) (US)
  - Suporta TLS/SSL: Sim
  
- **Matomo | Referrer Spam**
  - Autor: ([Matomo](http://github.com/matomo-org))
  - Repositório: [matomo-org/referrer-spam-blacklist](http://github.com/matomo-org/referrer-spam-blacklist) (GitHub)
  - Licença: Sem licença formal (Freeware)
  - Download: [raw.githubusercontent.com](http://raw.githubusercontent.com/matomo-org/referrer-spam-blacklist/master/spammers.txt) (US)
  - Suporta TLS/SSL: Sim
  
- **Cameleon | Hosts List**
  - Autores: Philippe Bourcier e Stéphane Thiell.
  - Homepage: [sysctl.org](http://sysctl.org/cameleon)
  - Licença: Sem licença formal (Freeware)
  - Download: [sysctl.org](http://sysctl.org/cameleon/hosts) (FR)
  - Suporta TLS/SSL: Não

- **BarbBlock | Hosts File**
  - Autor: Paul Butler ([paulgb](http://github.com/paulgb))
  - Repositório: [paulgb/BarbBlock](http://github.com/paulgb/BarbBlock) (GitHub)
  - Licença: [MIT License](http://github.com/paulgb/BarbBlock/blob/master/LICENSE)
  - Download: [raw.githubusercontent.com](http://raw.githubusercontent.com/paulgb/BarbBlock/master/blacklists/hosts-file.txt) (US)
  - Suporta TLS/SSL: Sim

- **Steven Black | Unified hosts = (adware + malware)**
  - Autor: Steven Black ([StevenBlack](http://github.com/StevenBlack))
  - Repositório: [StevenBlack/hosts](http://github.com/StevenBlack/hosts) (GitHub)
  - Licença: [MIT License](http://github.com/StevenBlack/hosts/blob/master/license.txt)
  - Download: [raw.githubusercontent.com](http://raw.githubusercontent.com/StevenBlack/hosts/master/hosts) (US)
  - Suporta TLS/SSL: Sim

- **Steven Black | Unified hosts + gambling + social**
  - Autor: Steven Black ([StevenBlack](http://github.com/StevenBlack))
  - Repositório: [StevenBlack/hosts](http://github.com/StevenBlack/hosts) (GitHub)
  - Licença: [MIT License](http://github.com/StevenBlack/hosts/blob/master/license.txt)
  - Download: [raw.githubusercontent.com](http://raw.githubusercontent.com/StevenBlack/hosts/master/alternates/gambling-social/hosts) (US)
  - Suporta TLS/SSL: Sim
  
- **AdAway | Hosts List**
  - Autor: Bruce BUJON ([PerfectSlayer](http://github.com/PerfectSlayer))
  - Homepage: [adaway.org](http://adaway.org)
  - Licença: [CC BY 3.0 ](http://creativecommons.org/licenses/by/3.0)
  - Download: [adaway.org](http://adaway.org/hosts.txt) (US)
  - Suporta TLS/SSL: Sim
  
- **Tiuxo | Hosts List**
  - Autor: Brian Clemens ([tiuxo](http://github.com/tiuxo))
  - Repositório: [tiuxo/hosts](http://github.com/tiuxo/hosts)  (GitHub)
  - Licença: [CC BY 4.0](http://github.com/tiuxo/hosts/blob/master/LICENSE)
  - Download: [raw.githubusercontent.com](http://raw.githubusercontent.com/tiuxo/hosts/master/ads) (US)
  - Suporta TLS/SSL: Sim
  
- **PornHosts | Hosts File**
  - Autor: Niclas ([Clefspeare13](http://github.com/Clefspeare13))
  - Repositório: [Clefspeare13/pornhosts](http://github.com/Clefspeare13/pornhosts) (GitHub)
  - Licença: [MIT License](http://github.com/Clefspeare13/pornhosts/blob/master/LICENSE)
  - Download: [raw.githubusercontent.com](http://raw.githubusercontent.com/Clefspeare13/pornhosts/master/0.0.0.0/hosts) (US)
  - Suporta TLS/SSL: Sim
  
- **PornAway | Hosts File**
  - Autor: Mahadi Xion ([mhxion](http://github.com/mhxion))
  - Repositório: [mhxion/pornaway](http://github.com/mhxion/pornaway) (GitHub)
  - Licença: [MIT License](http://github.com/mhxion/pornaway/blob/master/LICENSE)
  - Download: [raw.githubusercontent.com](http://raw.githubusercontent.com/mhxion/pornaway/master/hosts/porn_sites.txt) (US)
  - Suporta TLS/SSL: Sim

- **StevenBlack | Porn Hosts**
  - Autor: Steven Black ([StevenBlack](http://github.com/StevenBlack))
  - Repositório: [StevenBlack/hosts](http://github.com/StevenBlack/hosts) (GitHub)
  - Licença: [MIT License](http://github.com/StevenBlack/hosts/blob/master/license.txt)
  - Download: [raw.githubusercontent.com](http://raw.githubusercontent.com/StevenBlack/hosts/master/alternates/porn/hosts) (US)
  - Suporta TLS/SSL: Sim

<h3 align='center'>Softwares de terceiros</h3>

Abaixo está disponível uma lista de todos os softwares de terceiros que estão atualmente sendo utilizados no Simple AdAway:

- **Unity (Un)Installer Template**
  - Autores: [ahrion](http://github.com/therealahrion) e [Zackptg5](http://github.com/Zackptg5)
  - Repositório: [Zackptg5/Unity](http://github.com/Zackptg5/Unity) (GitHub)
  - Licença: [GNU General Public License v2.0](http://github.com/Zackptg5/Unity/blob/master/LICENSE)
  - Download: [Zackptg5/Unity](http://codeload.github.com/Zackptg5/Unity/zip/master)

- **Move Certificates**
  - Autor: [yochananmarqos](http://github.com/yochananmarqos)
  - Repositório: [Magisk-Modules-Repo/movecert](http://github.com/Magisk-Modules-Repo/movecert) (GitHub)
  - Licença: Sem licença formal (Freeware)
  - Download: [Magisk-Modules-Repo/movecert](http://codeload.github.com/Magisk-Modules-Repo/movecert/zip/master)

- **CloudflareDNS4Magisk**
  - Autor: [Rom](http://github.com/xerta555)
  - Repositório: [Magisk-Modules-Repo/CloudflareDNS4Magisk](http://github.com/Magisk-Modules-Repo/CloudflareDNS4Magisk) (GitHub)
  - Licença: [GNU Lesser General Public License v3.0](http://github.com/Magisk-Modules-Repo/CloudflareDNS4Magisk/blob/master/LICENSE)
  - Download: [Magisk-Modules-Repo/CloudflareDNS4Magisk](http://codeload.github.com/Magisk-Modules-Repo/CloudflareDNS4Magisk/zip/master)

- **CloudflareDNS4Magisk-IPv6**
  - Autor: [Rom](http://github.com/xerta555)
  - Repositório: [Magisk-Modules-Repo/CloudflareDNS4Magisk-IPv6](http://github.com/Magisk-Modules-Repo/CloudflareDNS4Magisk-IPv6) (GitHub)
  - Licença: [GNU Lesser General Public License v3.0](http://github.com/Magisk-Modules-Repo/CloudflareDNS4Magisk-IPv6/blob/master/LICENSE)
  - Download: [Magisk-Modules-Repo/CloudflareDNS4Magisk](http://codeload.github.com/Magisk-Modules-Repo/CloudflareDNS4Magisk-IPv6/zip/master)

O Simple AdAway só é possível graças a essas pessoas que disponibilizaram seus trabalhos como software livre e permitiram que outras pessoas pudessem usá-los, portanto, todos os créditos e agradecimentos vão para eles.
