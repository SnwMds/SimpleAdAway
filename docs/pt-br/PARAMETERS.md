_Siga [estas instruções](README.md#execu%C3%A7%C3%A3o-com-par%C3%A2metros) para saber como executar o Simple AdAway utilizando os parâmetros à seguir._  

<h4 align='center'>Parâmetros básicos</h4>

---
* `--setup`

Configura o ambiente de operações e cria todos os diretórios e arquivos do Simple AdAway no armazenamento interno. O Simple AdAway já executa esta operação automaticamente quando necessário. Este parâmetro é destinado apenas para testes.

**Execução:** `sad --setup` ou `sh '/sdcard/sad' --setup`

---
* `--all`

Realiza o download e faz a aplicação de todos os 15 filtros disponíveis (não incluí filtros anti-pornografia). Por padrão, o Simple AdAway realiza o download de apenas 5 filtros. Em teoria, isso já é o suficiente para bloquear a grande maioria dos conteúdos indesejados. Utilize este parâmetro apenas se anúncios ou outros conteúdos do gênero continuem a ser exibidos a você mesmo com a aplicação dos filtros padrões.

**Execução:** `sad --all` ou `sh '/sdcard/sad' --all`

---
* `--include-porn`

Realiza o download e faz a aplicação de todos os 3 filtros anti-pornografia disponíveis. Estes filtros podem ser aplicados em conjunto com todos os outros filtros (exemplo: ALL, DEFAULT ou CUSTOM).

**Execução:** `sad --include-porn` ou `sh '/sdcard/sad' --include-porn`

---
* `--restore-all` 

Remove todo o bloqueio de hosts aplicado ao sistema, restaura os servidores DNS padrões (caso tenham sido previamente aplicados) e remove todas as regras customizadas que foram aplicadas utilizando o iptables/ip6tables. Note que esta operação não removerá o script executável do Simple AdAway de seu sistema ou armazenamento interno. Isto também não removerá as regras customizadas salvas em "**/sdcard/SimpleAdAway/Rules**".

**Execução:** `sad --restore-all` ou `sh '/sdcard/sad' --restore-all`

---
* `--skip` 

Pula as verificações iniciais do root, busybox e da conexão com a internet. Às demais verificações continuarão a ser executadas, pois elas são necessárias para que o Simple AdAway identifique seu ambiente de operações e funcione corretamente.

**Execução:** `sad --skip` ou `sh '/sdcard/sad' --skip`

---
* `--help` 

Exibe uma descrição e uma explicação prévia sobre o script e seus respectivos parâmetros de execução.

**Execução:** `sad --help` ou `sh '/sdcard/sad' --help`

---
* `--version` 

Exibe informações sobre o autor e a versão atual do Simple AdAway.

**Execução:** `sad --version` ou `sh '/sdcard/sad' --version`

---
* `--interactive `

Entra no modo interativo do Simple AdAway. Neste modo, é possível executar operações específicas sem que seja necessário utilizar parâmetros adicionais. No modo interativo, a coleta de logs é ativada automaticamente.

**Execução:** `sad --interactive` ou `sh '/sdcard/sad' --interactive`

---
* `--clear-dns`

Restaura/Remove todas configurações relacionadas a alteração de DNS (IPv4/IPv6).

**Execução:** `sad --clear-dns` ou `sh '/sdcard/sad' --clear-dns`

---
* `--clear-blacklist`

Restaura/Remove todas configurações relacionadas a lista negra customizada (Hosts/IPv4/IPv6). Isto não inclui o ficheiro de hosts do sistema.

**Execução:** `sad --clear-blacklist` ou `sh '/sdcard/sad' --clear-blacklist`

---
* `--clear-whitelist`

Restaura/Remove todas configurações relacionadas a lista branca customizada (Hosts/IPv4/IPv6). Isto não inclui o ficheiro de hosts do sistema.

**Execução:** `sad --clear-whitelist` ou `sh '/sdcard/sad' --clear-whitelist`

---

<h4 align='center'>Parâmetros avançados</h4>

---
* `--force-https`

Faz com que todas as conexões via `wget` sejam realizadas utilizando o protocolo [https](http://pt.m.wikipedia.org/wiki/Hyper_Text_Transfer_Protocol_Secure). Por padrão, todas as conexões via `wget` são realizadas utilizando o protocolo http. Não é recomendado utilizar este parâmetro como padrão, pois alguns servidores não oferecem suporte nativo ao **TLS/SSL**. Forçar o uso do https nesses servidores fará com que a conexão seja recusada, causando assim uma falha no download dos arquivos.

**Execução:** `sad --force-https` ou `sh '/sdcard/sad' --force-https`

---
* `--enable-logging`

Faz com que todos os logs de execução do Simple AdAway, bem como os logs do sistema e informações sobre o dispositivo, sejam armazenados em um arquivo compactado nomeado como "**SimpleAdAway.tar.bz2**" no diretório de logs do Simple AdAway no armazenamento interno `( /sdcard/SimpleAdAway/Logs )`. Isso pode ser utilizado para fins de teste ou para relatar erros e/ou problemas de execução.

**Execução:** `sad --enable-logging` ou `sh '/sdcard/sad' --enable-logging`

---
*  `--ipv6-only`

Diz ao Simple AdAway que ele deve dar preferência a configurações relacionadas ao IPv6. Este parâmetro não interfere na forma como as regras personalizadas dos arquivos em `( /sdcard/SimpleAdAway/Rules )` são aplicadas.

**Execução:** `sad --ipv6-only` ou `sh '/sdcard/sad' --ipv6-only`

---
* `--use-blacklist`

Bloqueia as hosts e os endereços IPv4/IPv6 especificados nos arquivos "**Hosts.txt**", "**IPv4.txt**" e "**IPv6.txt**" `( /sdcard/SimpleAdAway/Rules/Blacklists )`. Leia os comentários descritos nesses arquivos para obter mais informações sobre como utilizar este parâmetro.

**Execução:** `sad --use-blacklist` ou `sh '/sdcard/sad' --use-blacklist`

---
* `--use-whitelist`

Autoriza o acesso a hosts e endereços IPv4/IPv6 especificados nos arquivos "**Hosts.txt**", "**IPv4.txt**" e "**IPv6.txt**" `( /sdcard/SimpleAdAway/Rules/Whitelists )`. Caso estas hosts e/ou endereços IP já estejam bloqueados, os mesmos serão removidos na próxima vez que você executar o Simple AdAway.  Leia os comentários descritos nesses arquivos para obter mais informações sobre como utilizar este parâmetro.

**Execução:** `sad --use-whitelist` ou `sh '/sdcard/sad' --use-whitelist`

---
* `--use-custom-sources`

Realiza o download de arquivos de hosts das fontes especificadas no arquivo "**Sources.txt**" `( /sdcard/SimpleAdAway/Sources )` . Leia os comentários descritos nesse arquivo para obter mais informações sobre como utilizar este parâmetro. Note que o parâmetro `--force-https` não se aplica a fontes de hosts personalizadas. O protocolo usado durante a conexão será aquele que você mesmo especificou. Exemplo: se a URL que você inseriu começar com _https://_, a conexão será realizada utilizando o protocolo **TLS/SSL**. Se começar com _http://_, será realizada utilizando o protocolo http padrão, e vice-versa.

**Execução:** `sad --use-custom-sources` ou `sh '/sdcard/sad' --use-custom-sources`

---
* `--custom-settings`

Aplica todas as configurações especificadas no arquivo "**Settings.txt**" `( /sdcard/SimpleAdAway/Settings )` . Para obter mais informações sobre como utilizar este parâmetro, leia os comentários descritos no arquivo especificado.

**Execução:** `sad --custom-settings` ou `sh '/sdcard/sad' --custom-settings`

---
* `--uninstall`

Remove todos os arquivos criados pelo Simple AdAway em seu sistema e armazenamento interno. Esta ação também irá restaurar todas as configurações modificadas previamente pelo Simple AdAway.

**Execução:** `sad --uninstall` ou `sh '/sdcard/sad' --uninstall`

---
*  `--clean-cached`

Limpa o cache de todos os aplicativos instalados.

**Execução:** `sad --clean-cached` ou `sh '/sdcard/sad' --clean-cached`

---
*  `--cleanup`

Limpa arquivos temporários deixados pelo Simple AdAway no armazenamento interno.

**Execução:** `sad --cleanup` ou `sh '/sdcard/sad' --cleanup`

---

<h4 align='center'>Alteração de DNS</h4>

---
* `--use-cloudflare-dns`

Realiza a aplicação do serviço de DNS da CloudFlare.

- Vantagens
  - Boa velocidade e tempo de resposta.
  - Suporta IPv6.
  - Possuí servidores no Brasil.

- Desvantagens
  - Coleta [parcialmente](http://developers.cloudflare.com/1.1.1.1/commitment-to-privacy/privacy-policy/privacy-policy) alguns dados de tráfego.
  - É [anycast](https://pt.m.wikipedia.org/wiki/Anycast) (encaminha seu tráfego entre vários servidores).

**Execução:** `sad --use-cloudflare-dns` ou `sh '/sdcard/sad' --use-cloudflare-dns`

---
* `--use-adguard-dns`

Realiza a aplicação do serviço de DNS do AdGuard.

- Vantagens
  - Bloqueia o acesso a domínios de anúncios, analytics e malware.
  - Suporta IPv6.
  - Não rastreia sua atividade online.

- Desvantagens
  - Não possuí servidores no Brasil.
  - É [anycast](https://pt.m.wikipedia.org/wiki/Anycast) (encaminha seu tráfego entre vários servidores).

**Execução:** `sad --use-adguard-dns` ou `sh '/sdcard/sad' --use-adguard-dns`

---
* `--use-open-dns`

Realiza a aplicação do serviço de DNS da OpenDNS.

- Vantagens
  - Bloqueia o acesso a domínios de phishing, malware e sites clonados.
  - Suporta IPv6.

- Desvantagens
  - O OpenDNS monitora sua atividade online.
  - Não possuí servidores no Brasil.

**Execução:** `sad --use-open-dns` ou `sh '/sdcard/sad' --use-open-dns`

---
* `--use-google-dns`

Realiza a aplicação do serviço de DNS da Google. 

- Vantagens
  - Oferece proteção contra ataques de phishing e DDoS.
  - Suporta IPv6.

- Desvantagens
  - O Google monitora sua atividade online.
  - Não possuí servidores no Brasil.

**Execução:** `sad --use-google-dns` ou `sh '/sdcard/sad' --use-google-dns`

---
* `--use-uncensored-dns`

Realiza a aplicação do serviço de DNS da UncensoredDNS 

- Vantagens
  - É um serviço de DNS que visa a não-censura na internet.
  - Não rastreia sua atividade online.
  - Suporta IPv6.

- Desvantagens
  - Não possuí servidores no Brasil.

**Execução:** `sad --use-uncensored-dns` ou `sh '/sdcard/sad' --use-uncensored-dns`

---
* `--use-keweon-dns`

Realiza a aplicação do serviço de DNS da keweonDNS 

- Vantagens
  - Bloqueia o acesso a domínios de anúncios, analytics e malware.
  - Suporta IPv6.
  - Não rastreia sua atividade online.

- Desvantagens
  - Não possuí servidores no Brasil.

**Execução:** `sad --use-keweon-dns` ou `sh '/sdcard/sad' --use-keweon-dns`

---
* `--use-quad9-dns`

Realiza a aplicação do serviço de DNS da Quad9 

**Execução:** `sad --use-quad9-dns` ou `sh '/sdcard/sad' --use-quad9-dns`

---
* `--use-level3-dns`

Realiza a aplicação do serviço de DNS da Level3 

**Execução:** `sad --use-level3-dns` ou `sh '/sdcard/sad' --use-level3-dns`

---
* `--use-verisign-dns`

Realiza a aplicação do serviço de DNS da Verisign 

**Execução:** `sad --use-verisign-dns` ou `sh '/sdcard/sad' --use-verisign-dns`

---
* `--use-dns-watch`

Realiza a aplicação do serviço de DNS da DNS.WATCH 

**Execução:** `sad --use-dns-watch` ou `sh '/sdcard/sad' --use-dns-watch`

---
* `--use-comodo-dns`

Realiza a aplicação do serviço de DNS da Comodo 

**Execução:** `sad --use-comodo-dns` ou `sh '/sdcard/sad' --use-comodo-dns`

---
* `--use-advantage-dns`

Realiza a aplicação do serviço de DNS da Advantage 

**Execução:** `sad --use-advantage-dns` ou `sh '/sdcard/sad' --use-advantage-dns`

---
* `--use-greenteam-dns`

Realiza a aplicação do serviço de DNS da GreenTeamDNS 

**Execução:** `sad --use-greenteam-dns` ou `sh '/sdcard/sad' --use-greenteam-dns`

---
* `--use-safe-dns`

Realiza a aplicação do serviço de DNS da SafeDNS 

**Execução:** `sad --use-safe-dns` ou `sh '/sdcard/sad' --use-safe-dns`

---
* `--use-opennic-dns`

Realiza a aplicação do serviço de DNS da OpenNIC 

**Execução:** `sad --use-opennic-dns` ou `sh '/sdcard/sad' --use-opennic-dns`

---
* `--use-smartviper-dns`

Realiza a aplicação do serviço de DNS da SmartViper 

**Execução:** `sad --use-smartviper-dns` ou `sh '/sdcard/sad' --use-smartviper-dns`

---
* `--use-freenom-dns`

Realiza a aplicação do serviço de DNS da Freenom 

**Execução:** `sad --use-freenom-dns` ou `sh '/sdcard/sad' --use-freenom-dns`

---

Através do modo interativo você tem acesso à diversas outras opções e configurações.

_**Aviso:** Apenas use os parâmetros `--version`, `--help`, `--setup`, `--interactive`, `--clear-dns`, `--clear-blacklist`, `--clear-whitelist`, `--uninstall` e `--restore-all` individualmente. Esses parâmetros em específico são funções únicas, isto significa que elas não podem e não devem ser executadas juntas com às demais funções._

_**Aviso (2):** Os únicos parâmetros que podem ser chamados em conjunto com o `--interactive` são: `--custom-settings`, `--ipv6-only`, `--enable-logging` e `--force-https`. Caso você tente chamar algum outro parâmetro que não esteja listado aqui, o mesmo será ignorado._

_**Aviso (3):** Minha rede não suporta o protocolo `IPv6`, portanto eu não fui capaz de realizar testes utilizando todas as funções relacionadas a esse protocolo. Em poucas palavras, funções como o "**DNS Changer**" e "**Blacklist/Whitelist**" não foram testadas em outros protocolos além do próprio `IPv4`. Isso significa que, se você usa apenas este protocolo para acessar a internet, algumas configurações do Simple AdAway podem não funcionar e/ou não serem com o seu ambiente de operações. Em todo caso, use com cautela._
