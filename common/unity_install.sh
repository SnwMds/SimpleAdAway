# Set "Uninstall" function
Uninstall(){
	rm -rf '/system/bin/sad' '/data/adb/modules/SimpleAdAway' '/sbin/.magisk/modules/SimpleAdAway' '/sdcard/SimpleAdAway' '/sdcard/sad' '/system/etc/init.d/set-dns-at-boot.sh' '/su/su.d/set-dns-at-boot.sh' '/data/system/SimpleAdAway' /system/addon.d/*SimpleAdAway*; sed -i '/.dns/d' '/system/build.prop'
}

# Detect if Magisk is installed and verify if the Systemless Hosts Module is enabled
if [ -d '/data/adb/magisk' ]; then
	[ -d '/data/adb/modules/hosts' ] || { ui_print "* The systemless hosts module isn't enabled!"; Uninstall; abort '* Enable it in Magisk Settings before proceeding!'; }
fi

# Search for other DNS-related modules
[ -d /data/adb/modules/*DNS* ] && ui_print '* Other DNS changers modules were found!' && Uninstall && abort '* Please, remove it before proceeding!'

# Unset function
unset 'Uninstall'

