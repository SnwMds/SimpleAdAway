# Set "Uninstall" function
Uninstall(){
	rm -rf '/system/bin/sad' '/data/adb/modules/SimpleAdAway' '/sbin/.magisk/modules/SimpleAdAway' '/sdcard/SimpleAdAway' '/sdcard/sad' '/system/etc/init.d/set-dns-at-boot.sh' '/su/su.d/set-dns-at-boot.sh' '/data/system/SimpleAdAway' /system/addon.d/*SimpleAdAway*; sed -i '/.dns/d' '/system/build.prop'
	echo -e '127.0.0.1 localhost\n::1localhost' > '/data/adb/modules/hosts/system/etc/hosts'; echo -e '127.0.0.1 localhost\n::1localhost' > '/system/etc/hosts'
	chmod '644' '/data/adb/modules/hosts/system/etc/hosts'; chmod '644' '/system/etc/hosts'
	chown 'root:root' '/data/adb/modules/hosts/system/etc/hosts'; chown 'root:root' '/system/etc/hosts'
	ndc resolver flushif; ndc resolver flushdefaultif; ndc resolver clearnetdns rmnet0; ndc resolver clearnetdns wlan0
	iptables -F; iptables -X; ip6tables -F; ip6tables -X; iptables -t nat -X; iptables -t nat -F; ip6tables -t nat -X; ip6tables -t nat -F
	iptables -t mangle -F; iptables -t mangle -X; iptables -P INPUT ACCEPT; iptables -P OUTPUT ACCEPT; iptables -P FORWARD ACCEPT
	ip6tables -t mangle -F; ip6tables -t mangle -X; ip6tables -P INPUT ACCEPT; ip6tables -P OUTPUT ACCEPT; ip6tables -P FORWARD ACCEPT 
}

# Run and unset
Uninstall; unset 'Uninstall'

